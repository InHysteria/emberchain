# Quests
## Active

/// details | Prime Directive
	open: True

- One of..
	* [ ] Evacuate as many people as you can from earth before it's inevitable destruction.
	* [ ] Prevent the destruction of earth.
///

/// details | Diplomatic Arrangements
	open: True

- Introduce yourself too and reach a peaceful coexistance with..
	* [X] The Union
	* [X] The Network
	* [X] The Engineers of Ascension
///

/// details | Reach Bronze Rank
	open: True

- [X] Awaken all essence abilities
[=100% "20/20"]
- [ ] Raise all essence abilities to bronze rank
[=30% "6/20"]
///

/// details | The Summit
	open: True

- [X] Train with Faith in preparation for the climb
- [ ] Reach the peak of Mount Celeste
- Bonus Objective
	* [ ] Ensure all other expedition members reach the peak under their own power
[=0% "0/4"]
///

/// details | Jumpscouts Award for Proficiency, Silver (Any 3)
	open: True
- [ ] Perform another Heist
- [ ] Inflict 100 points of pun damage 
[=1% "0/100"]
- [ ] Complete a Quest in the worst way possible
- [ ] Go on Vacation
- [ ] Collect 3 cosplays
- [ ] Frustrate 3 distinct major powers
///

## Completed
/// details | Getting your bearings
- [X] Observe your surroundings
- [X] Familiarise yourself with my quest system

- Rewards
	* [X] [Warm Outdoors Wear]
	* [X] Choice of..
		+ [Jory's Own, Bottomshelf Hair Growth Elixir - Seriously Short]
		+ [Jory's Own, Bottomshelf Hair Growth Elixir - Vanilla Variant]
		+ [Jory's Own, Bottomshelf Hair Growth Elixir - Simply Shoulder Length]
		+ [Jory's Own, Bottomshelf Hair Growth Elixir - The Princess Special]
		+ [Jory's Own, Bottomshelf Hair Growth Elixir - Maximum Strength]
///

/// details | Basic survival
- [X] Identify where you are
- [X] Observe the natives
- [X] Find potable water & food
- [X] Find shelter

- Rewards
	* [X] 30 [Lesser Spirit Coin]
	* [X] 1,000 [United States Dollars]
///

/// details | Making friends
- [X] Ingratiate yourself with the natives
[=100% "5/5"]

- Rewards
	* [X] Choice of 1 of 3 random common essences
///

/// details | Armed and Ready
- [X] Procure a firearm
- Rewards
	* [X] [Gun Essence]
///

/// details | Theres Always a Bigger Fish
- [X] Encounter the agent of another organisation of at least bronze rank
- Rewards
	* [X] Choice of 1 of 3 weapons
///

/// details | Into the Breach
- [X] Discover a proto-astral space
- [X] Breach the boundary and resolve the dungeon
- Rewards
	* [X] Choice of 1 of 3 Skills Books
	* [X] Choice of 1 of 3 Rare Awakening Stones
	* [X] 200 [Iron Spirit Coins]
	* [X] 100 [eXpertise Points]
///

/// details | A Taste of Magic
- [X] Acquire an essence
- Rewards
	* [X] 50 [Lesser Spirit Coins]
	* [X] 20 [Iron Spirit Coins]
	* [X] [Mana Condenser, Iron]
	* [X] [Beginner Ritual Casting, Skill Book]
///

/// details | A Step into the Unknown
- [X] Attune an essence
- Rewards
	* [X] 5 [Iron Spirit Coins]
	* [X] [Awakening Stone of the Apprentice]
///

/// details | MVP (Minimum Viable Party)
- [X] Form a party of at least 4 people. (The fire system counts as 1 person for this task)
- Rewards
	* [X] [Awakening Stone of the Adventurer]
///

/// details | Red Pilled
- [X] Speak plainly with a native about your circumstances
- [X] Attempt to teach a native to utilise a power source from another iteration
- Rewards
	* [X] Choice of..
		+ [Awakening Stone of the Adventurer]
		+ [Awakening Stone of the Missionary]
		+ [Awakening Stone of the Savior]
///

/// details | A Larger Box
- [x] Empower yourself beyond normal means
- Rewards
	* [X] [Token of The Fox]
///

/// details | Jumpscouts Award for Proficiency, Bronze (Any 5)
	open: True
- [ ] Solve a problem by misleading those involved about your relative power level
- [X] Perform a Heist
- [X] Make 5 friends
[=100% "5/5"]
- [ ] Make 10 enemies
[=5% "1/10"]
- [ ] Make 15 frenemies
[=20% "3/15"]
- [X] Attain (relatively) vast cosmic power
- [X] Go on an Expedition
- [ ] Collect 3 notable keepsakes
[=66% "2/3"]
- [ ] Start a cult, sect, or other form of secret order
- [X] Frustrate 5 distinct local powers
[=100% "5/5"]
	* The Network
	* The Japanese Government
	* Knowledge
	* The Magic Guild
	* The Australian Government

- Rewards
	* [ ] [Jumpscouts Badge, Bronze]
	* [ ] 200 [Bronze Spirit Coin]
	* [ ] 3 [Free Ability Level-up (Bronze)]
	* [ ] [Premium Jumpscout Grab Bag]
///