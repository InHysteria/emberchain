# Faith
Young american trans woman.

# Notable Connections
- [Brook](/characters/Brook) - Goddess
- [Ash](/characters/Fire Sisters) - Friend, Cognate
- [Lerin](/characters/Fire Sisters) - Weird Friend, Cognate, Creditor
- [Sadie](/characters/Fire Sisters) - Weird Friend, Cognate
- [Ember](/characters/Ember) - Weird Friend, Sister, Cognate
- [Lucy](/characters/Luciel) - Friend, Cognate
- [Skye](/characters/Skye) - Girlfriend, Cognate
- [Gregg](/characters/Gregg) - Childhood Friend, Cognate
- [Georgia](/characters/Georgia) - Childhood Friend?, Cognate
- [V / Vai](/characters/V) - Childhood Friend, Cognate, Home Server
- [Camille](/characters/Camille) - Compatriate, Cognate
- [Eve](/characters/secondary/Evelyn Sharp) - Online Friend
- [Katie](/characters/secondary/Katie Davis) - Online Friend
- [Amy](/characters/secondary/Amisra Elxisys) - Online Friend

# Energy Systems
## Essence User
### Gifts
/// details | Human Ambition
	type: minor
	open: True
```text
Essence abilities advance more quickly.
```
///
/// details | Special Attack Aptitude
	type: minor
	open: True
```text
You are more likely to awaken special attacks than other ability types. Your special attacks have increased effect.
```
///
/// details | Improvised Magic
	type: minor
	open: True
```text
Consume non-hostile magical phenomena to replenish your mana. This ability can overflow your mana capacity with increasing deleterious effects.
```
///
/// details | Sharpshooter
	type: minor
	open: True
```text
Your accuracy with firearms is greatly increased above the standard for your race.
```
///
/// details | Lightweight
	type: minor
	open: True
```text
You are one third the weight of a comparable member of your race.
```
///
/// details | Echo Shot
	type: minor
	open: True
```text
You can invest a large quantity of mana into a single shot from you gun, to cause it to carry the effects of several bullets.
```
///
### Essences
#### Magic
/// details | Identifiy
	type: minor
	open: True
```text
Iron: 
	Learn the name, purpose, and function of a entity.
Bronze:
	Learn the intention of an entity.

Entities of your rank or greater may resist any of these effects.
```
///
/// details | Gateway
	type: minor
	open: True
```text
Conjure a gateway nearby which connects to a distant space. The gateway is capable of transporting up to 10 people of your rank with higher ranks reducing and lower ranks increasing this count by a factor of 10. Mana cost is variable based on distance, with your maximum distance always consuming a substantial amount of mana.
The gateway lasts until it's capacity has been reached or it leaves your aura radius.

Iron: 
	Maximum distance 20km	
Bronze: 
	Maximum distance 2000km	
```	
///
/// details | Fireball
	"Bold words for someone standing in fireball range."

Bronze
///
/// details | Enact Ritual
	https://he-who-fights-with-monsters.fandom.com/wiki/Enact_Ritual

Iron
///
/// details | Tools of the Magister
	https://he-who-fights-with-monsters.fandom.com/wiki/Tools_of_the_Magister

Iron
///
#### Gun
/// details | Endless Magazine
	type: minor
	open: True
```text
Convert mana into temporary bullets with a variable mana cost and cooldown based on the calibre.

Iron:
	Empower your bullets with your other essence abilities. Multiple bullets can be empowered at a time with reduced effect
Bronze:
	Create exotic ammunition at a greater mana cost.
```
///
/// details | Black Widow
	type: minor
	open: True
```text
Conjure a lever action rifle detailed with silver metal and glossy black wood. 

Iron: 
	Bullets fired from this gun inflict [Widow's Mark] on the target.
Widow's Mark:
	Receive additional disruptive force damage from attacks made by the owner

Bronze: 
	Widow's Mark drains stamina from the target. Additional stacks have diminishing effect.
```
///
/// details | Anti-Tank Round
	type: minor
	open: True
```text
Embue a bullet with mana to deal additional resonating force.

Iron
```
///
/// details | Rubber Bullets
	type: minor
	open: True
```text
Reduce the overall damage of a bullet. When imbued this way, the bullet will never kill the target.

Bronze
```
///
/// details | Scope
	type: minor
	open: True
```text
For a moderate mana cost you can manifest a multipurpose scope which can be configured to your needs.

Iron:
	Thermal Vision
	Variable Zoom
	Range Finder

Bronze:
	Aura Vision
	Target Painting
```
///
#### Wing
/// details | Manifest Wings
	type: minor
	open: True
```text
For a large initial mana cost, manifest a large pair of wings. These wings require no mana to maintain and can be dismissed at any point. The wings are light physical appendages which you can control as easily as you would any other limb. Unaided, your wings are not sufficient to provide lift for your body.

Iron:
	You have in an instinctive sense on how to orientate and maneuver yourself in the air. 
	While your wings are manifested you can use them to leap a large distance, glide, and slow fall.
Bronze:
	For a small ongoing mana cost you can empower your wings to provide much greater lift giving you the ability to sustain flight.
```
///
/// details | Corvidae Contact - Gyle
	type: minor
	open: True
```text
Summon an intelligent crow for a very high initial mana cost and a 1 day cooldown. Once created the crow exists until dismissed. 
Your crow has heightened senses far outstripping that of any unenhanced bird. For a miniscule ongoing mana cost your crow can share its senses with you. 
Your crow can be absorbed into your body and while there it manifests as a cluster of black feathers on your head.

Iron:
	Your crow shares it's language with you, allowing you to speak with and understand other birds. Crows, Ravens, and other Corvidae see you as one of their own.
	Your crows perceptive abilities allow it to perceive magic.		
Bronze:
	Your crows perceptive abilities allow it to perceive auras.

```
///
/// details | Razor Feather
	type: minor
	open: True
```text
Manifest a volley of razor sharp feathers for a moderate mana cost. 
You can utilise the feathers on your manifested wings to reduce this cost substantially.
```
///
/// details | Slow Fall
	type: minor
	open: True
```text
Iron:
	Gravity has less of an effect on allies within your aura when their velocity is above a threshold. 
	Fall damage they recieve is reduced.	
Bronze:
	Fall damage is negated entirely.
```
///
/// details | Brookian Absolution
	type: minor
	open: True
```text
For a very large mana cost, absolve a target of their sins, offering them the protection of [Brook].
```
///
#### Deadshot
/// details | Marked for Death
	type: minor
	open: True
```text
Inflict a condition upon a foe which aids in tracking them and deals additional resonating force damage when hit by attacks from you or your allies. This affliction cannot be cleansed through normal means.

Iron:
	Instinctively know where a marked foe has travelled by following a trail they leave. 
```
///
/// details | Engraved Bullet
	type: minor
	open: True
```text
Manifest a special bullet bearing the name of your mark. This bullet can be empowered indefinitely and has no limit on the mana it can hold. 
This bullet will not fire unless the attack would hit.

Iron:
	Up to 4 engraved bullets may exist at a time.
	When this bullet is used to attack the designated target it deals additional transcendent damage based on how long it has existed and how much it has been empowered.
```
///
/// details | Executioner
	type: minor
	open: True
```text
For a moderate mana cost, manifest a special bullet which deals moderate transcendent damage. This damage is only applied if it would be sufficient to kill the target.

Iron
```
///
/// details | Bounty Hunter
	type: minor
	open: True
```text
You possess a dimensional storage space. Marked targets that you kill, are looted with a focus on currency and spirit coins.

Bronze
```
///
/// details | Lawbringer
	type: minor
	open: True
```text
Your sense of right and wrong extends to those around you. When someone transgresses in your presence, you may mark them at no cost. 
When you attack a marked target, you deal additional transcendent damage based on the severity of the transgression. 

Bronze
```
///