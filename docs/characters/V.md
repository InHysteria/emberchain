# V / Vai

Going through some things.

# Notable Connections
- [Faith](/characters/Faith) - Childhood Friend, Cognate
- [Gregg](/characters/Gregg) - Childhood Friend, Cognate
- [Georgia](/characters/Georgia) - Childhood Friend?, Cognate
- [Skye](/characters/Skye) - Friend, Cognate
- [Lucy](/characters/Luciel) - Friend, Cognate
- [Ash](/characters/Fire Sisters) - Friend, Cognated
- [Lerin](/characters/Fire Sisters) - Friend, Cognate, Creditor
- [Sadie](/characters/Fire Sisters) - Friend, Cognate
- [Ember](/characters/Luciel) - Girl Friend, Cognate
- [VIChan](/redacted) - Friend, Mentor
- [Camille](/characters/Camille) - Compatriate, Cognate

# Energy Systems

## Essence User
### Gifts
/// details | Human Ambition
	type: minor
	open: True
```text
Essence abilities advance more quickly.
```
///
/// details | Special Attack Aptitude
	type: minor
	open: True
```text
You are more likely to awaken special attacks than other ability types. Your special attacks have increased effect.
```
///
/// details | Aethan's Blessing
	type: minor
	open: True
```text
Your hair is always clean and well kept, at will you may spend mana to grow, shrink, or otherwise change your hair. 
```
///
/// details | Electronic Empathy
	type: minor
	open: True
```text
Your ability to empathise and extends to complex electronic devices. Effect is reduced for simple electronic devices.
```
///
/// details | Perceptive
	type: minor
	open: True
```text
Your perceptive abilities are heightened and perceptive abilities have increased effects.
```
///
/// details | VI Firmware 
	type: minor
	open: True
```text
Evolved from [Thread Dispatcher]
Your mind has been augmented by an advanced virtual intelligence causing you to operate in a similar fashion. Your ability to interact with digital consciousnesses is increased at the cost of reducing your analogue capabilities.
```
///
### Essences
#### Hair
/// details | Animate Hair
	type: minor
	open: True
```text
Your hair is a familiar bound to you physically. It is capable of rudimentary intelligence and will act according to your desires. 

Iron: 
	Your hair can harden sufficiently to perform weak but versatile attacks.

Bronze:
	Your hair can grow and shrink at up to 1 metre a minute.
```
///
/// details | Spider Climb
	type: minor
	open: True
```text
You may coat your body in enumerable microscopic hairs which cling to surfaces. 
When attempting to grip an object or surface, these hairs hold fast but also release at a thought. 

Iron:
	Perfect grip on any dry & rough surface.
```
///
/// details | Urticating Bristles
	type: minor
	open: True
```text
For a small mana cost you may grow specialised barbed hairs anywhere on your body. 
These hairs are coated in an irritant which causes great discomfort to any who make contact with them, and dissuades predators.

Iron:
	You may eject the hairs a short distance as a projectile attack.
```
///
/// details | Multi-tool
	type: minor
	open: True
```text
You can harden and form your hair into a variety of useful tools.

Iron:
	Screwdriver, Wrench, other basic non-complex tools.

Bronze:
	Files, knives, saws.
```
///
/// details | **Azriel's Shears**
	type: minor
	open: True
```text
For an all consuming mana cost, summon a weapon bearing a flawed impression of Azriel the Reaper, Seventh Judge of the Abidan Court.

"I don't know why I know these titles, nor why they scare me so. I don't know why this is so expensive, both in mana cost and the components required, nor do I know how VIChan got her hands (digital or otherwise) on said components. Finally I don't know why this is under the hair essence of all places... Perhaps it would be best that we don't try summoning these until we're very ready to deal with the consequences, perhaps never at all." - Akashic Codex 

"I've left my previous comment there for posterity, that being said I don't know how much I can add beyond that he's bronze rank now apparently, no idea how that happened." - Akashic Codex 
```
///
#### Tech
/// details | Interface
	type: minor
	open: True
```text
You are capable of interacting with all manner of technology through physical connection.

Iron:
	Electronic devices who's intended function is to be connected with. (Laptops, Security Cameras, etc)
```
///
/// details | Computational Mindset
	type: minor
	open: True
```text
~~You mind is much more efficient and handing arithmetic, calculating probabilities, and  expressing problems numerically.

Your proficiency with mathematics and logic is greatly increased.~~
"VIChan has tinkered with this quite a bit. I think it should now read.." - Akashic Codex
Your mind is better described as a discrete series of multi purpose cores capable of limited independent calculation. A complex series of interactions between these cores constitutes your mind.

Bronze
```
///
/// details | Co-processing
	type: minor
	open: True
```text
~~You have an additional processing unit attached to your mind. It is capable of handling basic tasks for you.~~
"VIChan has also messed around with this, I can't make heads or tails of it though." - Akashic Codex

Bronze
```
///
/// details | Bespoke Manufacturing
	type: minor
	open: True
```text
With the requisite knowledge you can manufacture complex devices traditionally only produced by precision machinery. You can substitute mana for a variety of tools and environments.

Iron:
	Welding, PnP, ISO 4 Clean Room Environments
```
///
/// details | Reverse Engineer
	type: minor
	open: True
```text
Dismantle devices for a variety of effects.

Iron:
	Understand the purpose and construction of dismantled devices.

Bronze:
	After dismantling several of the same device, you may spend a moderate amount of mana to create knowledge sufficient to reproduce a device given the required components.
```
///
#### Eye
/// details | Heightened Awareness
	type: minor
	open: True
```text
Your ability to perceive the world is increased. You may focus your attention on a specific spot to perceive in a level of detail comparable to specialist devices.

Iron:
	Focus equivalent to a jewellers magnifying glass.
```
///
/// details | Aurelius Aura
	type: minor
	open: True
```text
Your senses extends to everything within your Aura.

Iron:
	Sight, Sound
Bronze:
	Olfactory
```
///
/// details | Eye Blight
	type: minor
	open: True
```text
Iron:
	For a moderate mana cost temporarily blind a target.
```
///
/// details | Exotic Vision
	type: minor
	open: True
```text
For a variable ongoing mana cost, activate a variety of exist vision types.

Iron:
	Thermal, Magic, Aura
```
///
/// details | Eye Beams
	type: minor
	open: True
```text
For a moderate mana cost, project a damaging laser from your eyes.

Iron
```
///
#### Wetware
/// details | Sacrifical computing
	type: minor
	open: True
```text
Your physical form is suitable for operating as anologus to a traditional computer.
By over leveraging your capabilities you may permanently reconstruct portions of your body towards this end.

Bronze
```
///
/// details | Burn
	type: minor
	open: True
```text
Irreparably destroy any device of your rank or lower, removing all memory and rendering the device inoperable. 
Devices with a soul will instinctively resist this and will consider this a transgression.

Iron
```
///
/// details | Improvise
	type: minor
	open: True
```text
You can substitute some components for other components within their category.

Iron:
	Precious Metals, Adhesives, Plastics, etc.
	

"When you think about it Peanut Butter is a perfectly valid replacement for solder." - Jumpchan
```
///
/// details | Nebula Computing
	type: minor
	open: True
```text
You have access to a personal astral space where you can offload various computational resources.
Iron:
	Data storage

Bronze:
	Computation
```
///
/// details | Technicality
	type: minor
	open: True
```text
You recognise that there's no functional difference between electronic devices and "biological devices" and can consider the two to be relatively interchangeable.
Your abilities which apply to people, animals, and other living creatures can also apply to electronics, mechanical, and constructs.
Your abilities which apply electronics, mechanical, and constructs can also apply to people, animals, and other living creatures of one rank lower than yours.
```
///