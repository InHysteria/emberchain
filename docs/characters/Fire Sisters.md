# The Fire Sisters (Ash, Lerin, Sadie)
Once lone twin sister to Brook, now a system of three. 

# Notable Connections
- [Brook](/characters/Brook) - Sister
- [Ember](/characters/Ember) - Sister, Cognate
- [Faith](/characters/Faith) - Friend, Cognate
- [Lucy](/characters/Luciel) - Friend, Cognate
- [Skye](/characters/Skye) - Friend, Cognate
- [Gregg](/characters/Gregg) - Friend, Cognate
- [Georgia](/characters/Georgia) - Friend, Cognate
- [V / Vai](/characters/V) - Friend, Cognate, Home Server
- [Camille](/characters/Camille) - Compatriate, Cognate

# Energy Systems
/// details | Hexcasting
	open: True
```text
Technically capable and connected to the system but inexperienced.
```
///
## Essence User
### Gifts
/// details | Mindscape
	type: minor
	open: True
```text
Your thoughts are optimised and can be easily visualised.

When visualising thoughts, your ability to comprehend information is increased substantially at the expensive of speed and attention.

You may visual objects and phenomena unknown to you and experimentally discover new insights. This requires an example to be observed before hand and can only be used on phenomena of your rank or lower.
```
///
/// details | Astral Affinity
	type: minor
	open: True
```text
Increased resistance to the dimension effects and astral forces. Dimension abilities have increased effect and transcendent damage is increased.

"The magic here feels maleable." - Akashic Tome
```
///
/// details | Gestalt Mind 
	type: minor
	open: True
```text
You possess additional distinct minds capable of independent thought. Individually each mind functions at a reduced capacity relative to your rank, however when working together your gestalt mind is more capable than your peers.

Your body is controlled by a designated primary mind which may be changed at will.

Mental attacks made against you target the primary mind but all minds may contribute to the resistance.

Every mind can independently control your abilities.

You can duplicate your minds further with diminishing returns.

"I could see this being useful for study." - Akashic Codex
```
///
/// details | Blessing of the Fox
	type: minor
	open: True
```text
Portals and teleport abilities are more common, their powers increased and their deleterious effects are reduced.

You can speak and understand any spoken Language, with a teacher and a days effort this effect also extends to a language in it's written form.

Resist the wills of beings far outstripping your power more easily, the larger the power difference, the easier it is to ignore. 

Important and valuable aspects of the world are highlighted to you, this ability may be freely toggled without cost or cooldown.

This racial gift functions as a looting ability.

"I'm not entirely sure what a 'looting ability' is but I'm excited to find out." - Akashic Codex
```
///
/// details | Blessing of the Warehouse
	type: minor
	open: True
```text
The endless warehouse has taken a liking to you and offers it's blessing. 

You are capable of transporting items to and from the warehouse at will for a small mana cost, with larger and more conceptually dense items requiring larger expenditures. 

The warehouse contains a great many items belonging to your patron and they are inaccessible to this power.
"You should have a sense of it's contents innately but I'll keep an inventory for you as well." - Akashic Codex

Although by it's nature, the warehouse has no limits on storage space, the bond you share is comparatively weak and so only a small portion of it remains accessible to you when not within your Patron's domain.

"We go way back, I put in a good word for you." - Akashic Codex
```
///
/// details | Void Touched
	type: minor
	open: True
```text
Your soul has touched and been touched by the void between worlds and the process has left you marked in a manner obvious to those who know what to look for.

"We don't know much more about what this does I'm afraid, she's being awfully tight lipped about the whole process." - Akashic Codex
```
///
### Essences
#### Magic
/// details | Frugal Wizard
	type: minor
	open: True
```text
Iron: 
	Utilise speciality magic tools, vehicles and weapons. 
Bronze:
	All mana costs are reduced.
```
///
/// details | Neon Lights
	type: minor
	open: True
```text
Color spray
```
///
/// details | Invest Mana
	type: minor
	open: True
```text
Your can invest your mana into a variety of sources..

Iron:
	Trustfund: Invest your mana into a willing recipient. The target is inflicted with [Mana Debt].
	Launder: Condense a portion of your mana into Magic Quintessence of your rank and inflict yourself with Mana Debt. You can consume Magic Quintessence to recover mana.

Bronze: 
	Market Speculation: Invest your mana into an unwilling recipient, secretly making a prediction as you do.
		Bull Market: At any point, you may drain a percent of the targets mana corresponding to original investment.
		Bear Market: Inflict [Mana Debt] on the target when they use mana, proportionate to the original investment.

Mana Debt:
	A portion of the mana you recover is taken by the owner of this affliction. If you own this affliction the mana is lost instead. If this affliction is cleansed by any other effect, gain a stack of Fraud.
Fraud:
	The owner of this affliction knows your location at all times and they can treat you as the source for any of their abilities.
```
///
/// details | Investigate
	type: minor
	open: True
```text
For a variable mana cost, perform a through investigation upon an specific topic instantaneously.
Mana cost scales with the complexity of the investigation considering resources and existing knowledge, and on duration.

Iron:
	Up to 1 hour of investigation.
Bronze:
	Up to 1 day of investigation.
```
///
/// details | Mana Bomb
	type: minor
	open: True
```text
Spend a variable amount of mana to create a large explosion centred on you. As it's source you are immune to the effects of the explosion.

Iron:
	The amount of mana you may spend on this attack is equal to 10 times your natural capacity.
Bronze: 
	The amount of mana you may spend on this attack is equal to 100 times your natural capacity.
```
///
#### Dark
/// details | Midnight Eyes
	type: minor
	open: True
```text
Iron:
	See through darkness.
Bronze: 
	Sense magic.
```
///
/// details | Ghost Writer
	type: minor
	open: True
```text
Copies ritual magic that you have seen recently, with pseudo real effects.
Iron:
	Copies the effects of any written magic at reduced efficacy
```
///
/// details | Path of Shadows
	type: minor
	open: True
```text
Iron: 
	Teleport using shadows as a portal. You must be able to see the destination shadow. This effect is a special ability with a low mana cost and no cooldown. 
```
///
/// details | Pass without Trace
	type: minor
	open: True
```text
For a moderate ongoing mana cost, hide a target from a variety of senses.
You may cast this on multiple targets for an increased mana cost.

Iron:
	Sight, Sound
```
///
/// details | Traitorous Shadow
	type: minor
	open: True
```text
Hide in a targets shadow. You are invisible and incorporeal while within and have reduced senses.

Iron:
	You may whisper words only audible to the owner of the shadow.
```
///
#### Visage
/// details | Projection
	type: minor
	open: True
```text
For a variable mana cost, manifest project a quasi-real body. The projection will act autonomously if given instruction but may also be directly controlled. It is capable of utilising your own essence abilities, sharing your mana and cooldowns but with it's own vitality and stamina.
The projection has additional affects based on the material used to create it.		

Iron:
	Shadow Projection:
		Moderate cooldown, trivial mana cost, low ongoing mana cost which is reduced when in darkness or dim light conditions. 
		Project an amorphous shadowy form with reduced physical capabilties.
		Physical attacks made by the projection instead inflict Shadow Curse.
		Shadow Curse:
			Suffer ongoing necrotic damage to the target. The target gains additional stacks of Shadow Curse when in dim light or darkness, all stacks are lost when in direct daylight.

	Mana Projection:
		Long cooldown, moderate mana cost, moderate ongoing mana cost.
		Project a glowing body of mana incapable of physical interaction.
		Physical attacks made by the projection instead inflict Mana Drain. 

	Mana Drain:
		Suffer ongoing disruptive force damage. Damage inflicted by this condition restores the mana of it's owner.

Bronze:
	Simulacra:
		Long cooldown, very high mana cost, very low ongoing mana cost.
		Project a physical duplicate of yourself. 
		The simulacrum takes a few minutes to form and consumes a large quantity of mana, but requires little to sustain it afterwards.		
```
///
/// details | Shifter
	type: minor
	open: True
```text
For a variable mana cost, change your body shape to varying degrees for a time.

Iron:
	You can alter bodies features while keeping a consistent general shape. These effects persist for up to 8 hours.
Bronze:
	You are now capable of more extreme changes provided your total mass does not change by 25% in either direction.
	Normal effects can now be maintained indefinately.

```
///
/// details | Facsimile
	type: minor
	open: True
```text
For a very large mana cost, create a reproduction of anothers ability within your soul. The reproduced ability has increased mana cost and limited number of uses.

Iron:
	Copy an allies ability, with a small number of uses.
```
///
/// details | Unexpected Allies
	type: minor
	open: True
```text
Iron:
	You and your allies take on the illusionary form of nearby enemies, but your allies can still recognise one another. All allies and enemies in the area are randomly switch-teleported. 
```
///
/// details | Subjective Reality
	type: minor
	open: True
```text
For a moderate ongoing mana cost, create an illusory zone within your aura where you can freely control reality. 

Iron:
	Subjects of the illusion who are aware of it's nature, can overpower your control and block their perception of it by contesting your mana with their own.
Bronze:
	Your illusion can subtly confirm to the expectations of it's subjects, responding to their surface level thoughts.
```
///
#### Icon
/// details | Embrace Icon
	type: minor
	open: True
```text
For a low ongoing mana cost you may adapt yourself to an icon.
While you maintain effect, gradually gain stacks of "Adaptation".
You may use this ability multiple times with a short cooldown at reduced overall effectiveness.
Iron: 
	Icon of Speed:
		Enhance your bodies reflexes and physical speed.
	Icon of Power:
		Enhance your bodies physical strength.
	Icon of Recovery:
		Enhance your bodies natural recovery rate and toughness.

Bronze:
	Icon of Authority:
		Enhance your presence and the strength of your aura.
	Icon of Leadership:
		Allies within your aura are more resistant to hostile effects.

Adaptation:
	Your current form has additional potency, cumulative instances have an increase effect.
	Lose all stacks when switching forms, you may spend moderate mana for an instantaneous burst of potency for the new form.
```
///
/// details | Emblem
	type: minor
	open: True
```text
Manifest a representation of your icon. 

Bronze
```
///
/// details | Authority 
	type: minor
	open: True
```text
Impose your will upon anything contained by your aura.

Iron:
	Make a statement backed by mana about a non-sentient, non-magical entity within your aura.
```
///
/// details | Bestow
	type: minor
	open: True
```text
Grant your Adaptation stacks onto a target, these stacks are quickly lost but have their full potency until all are gone.
```
///
/// details | Venerate Icon
	type: minor
	open: True
```text
Take into yourself the ideals of your embraced icon, so long as you act in accordance with those ideals, you slowly gain additional stacks of adaptation at no mana cost.
If you act against your icon, you immediately lose all stacks of Adaptation. 

Bronze
```
///