# Camille
Wandering, Vampire
Loosely affiliated with the Union
Cognate

# Notable Connections
- [Lucy](/characters/Luciel) - Lover, Cognate
- [Faith](/characters/Faith) - Cognate
- [Skye](/characters/Skye) - Cognate
- [Gregg](/characters/Gregg) - Cognate
- [Georgia](/characters/Georgia) - Cognate
- [V / Vai](/characters/V) - Cognate
- [Ash](/characters/Fire Sisters) - Cognate
- [Lerin](/characters/Fire Sisters) - Cognate
- [Sadie](/characters/Fire Sisters) - Cognate
- [Ember](/characters/Ember) - Cognate
- [Lucy](/characters/Luciel) - Lover, Cognate
- [Iwaki Yomi](/characters/Iwaki Yomi) - Old Friend

# Energy Systems
## Invested Folkloric Entity
Bronze Rank, Vampire