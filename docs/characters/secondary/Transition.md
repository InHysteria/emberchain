# Transition
God of Progressive Change on Palimustus

Transition and [Brook](/characters/Brook) are connected in some confusing way, the way both [Jumpchan](/redacted) and [Brook](/characters/Brook) have tried to explain this is: 
```
Brook is Transition, but Transition is not Brook.
```

Though we've yet to recieve a clear explaination as to what that actually means.