# Builder, The
Great Astral Being
	
```
"Seems to like causing problems."
- Akashi Codex
```

# History

/// details | Taking Earth's Magic
	type: minor
	open: True

```
"Earth is (or was) created uniquely magically desolate, this was allegedly by the designs of the previous builder who created this situation for reasons unknown."
- Akashi Codex
```
///
/// details | Replaced
	type: minor
	open: True

```
"Perhaps related to their prior efforts in creating Earth, the Builder was allegedly replaced. What this means, and how this even worked we don't know."
- Akashi Codex
```
///
/// details | Network Founding
	type: minor
	open: True

```
"Sometime after taking the position the new builder sent one of their followers to Earth from Palimustus. This follower created a grand planet spanning array and charged humanity with it's operation and the caretaking of their planet. This follower was killed by the group that would eventually become known as the Network."
- Akashi Codex
```
///
/// details | Palimustus Incursion
	type: minor
	open: True

```
"While in Palimustus we learned of a builder cult comendeering and, through some unknown mechanism, completely disconnecting them from Palimustus, allegedly for the Builder."
- Akashi Codex
```
///