# Luciel (Lucy)
Latin american gender anarchist. Legal name Luciel but their friends call them Lucy for short.

# Notable Connections
- [Brook](/characters/Brook) - Goddess?
- [Faith](/characters/Faith) - Sister, Cognate
- [Skye](/characters/Skye) - Childhood Friend, Cognate
- [Gregg](/characters/Gregg) - Friend, Cognate
- [Georgia](/characters/Georgia) - Friend, Cognate
- [V / Vai](/characters/V) - Friend, Cognate, Home Server
- [Ash](/characters/Fire Sisters) - Sister, Cognate
- [Lerin](/characters/Fire Sisters) - Sister, Cognate, Creditor
- [Sadie](/characters/Fire Sisters) - Sister, Cognate
- [Ember](/characters/Ember) - Sister, Cognate
- [Camille](/characters/Camille) - Lover, Cognate

# Energy Systems
## Essence User
### Gifts
/// details | Human Ambition
	type: minor
	open: True
```text
Essence abilities advance more quickly.
```
///
/// details | Special Attack Aptitude
	type: minor
	open: True
```text
You are more likely to awaken special attacks than other ability types. Your special attacks have increased effect.
```
///
/// details | Cold Adaptation
	type: minor
	open: True
```text
Your temperature preference is lower than the baseline for your race, you thrive in cold environments and experience little discomfort from them. Conversely warm environments are less comfortable to you.
Extreme cold can be endured at the expense of mana.
This gift takes effect gradually when in a cold environment.
```
///
/// details | Grace of the Nightwalker
	type: minor
	open: True
```text
You have an effortless grace which others find Alluring and Mysterious. When in moonlight you have a subtle glow and this effect is enhanced.
This gift is negated in daylight.
```
///
/// details | Cinnamon Bun
	type: minor
	open: True
```text
People find it easy to like you, as long as you aren't hostile towards them. 
```
///
/// details | Pacifism
	type: minor
	open: True
```text
The potency of your healing abilities is increased dramatically. 
When you cause injuries or observe others doing so, this ability is temporarily disabled and is restored once you heal the injuries caused or when a day has passed.
```
///
### Essences
#### Ice
/// details | Ice Queens Glare
	type: minor
	open: True
```text
Infuse your aura with a sense of malice and hostility. Enemies within your aura find your presence intimidating, responding accordingly. 

Iron:
	You can condense your aura into an ephemeral semi-real blade with a continuous moderate mana cost.	
Bronze:
	For a moderate ongoing mana cost, you can periodically inflict [Paralyzed by Fear] on *all* everything within your aura that can percieve you.
	All stacks are lost when you stop using this ability.

Paralyzed by Fear:
	Targets inflicted with Paralyzed by Fear find it difficult to move and act. Repeated exposure and familiarity with the owner reduce this effect.
```
///
/// details | Frost Fairy - Crystal
	type: minor
	open: True
```text
Summon a small winged icy humanoid for a very high initial mana cost and a 1 day cooldown. Once created the clone exists until dismissed. 
The fairy possess an oversized mana capacity for it's rank however it is also fragile, only capable of withstanding a couple of attacks before being dismissed, when attacked it uses large quantities of mana to dissipate the impact and repair the damage.
The fairy can be absorbed into your body where it manifests as a pair of icy wings on your back, while absorbed the wings improve your speed and enable slow falling.

Iron: 
	The fairy can manifest ice at will and uses it to impale your enemies.

Bronze: 
	The fairies can flash freeze water.
```
///
/// details | Form from Ice
	type: minor
	open: True
```text
Create a stable and temperature resistant creation of ice. The creation can be further imbued with mana to enchance its effects and extend it's lifespan.

Iron: 
	Create a simple physical object comprised of a single discrete whole, the ice can be worked to hold delicate detailing and is strong enough to hold an edge.
```
///
/// details | Beckon the Frozen Wastes
	type: minor
	open: True
```text
Conjure a continual blizzard in a localised area. Snow and ice accumulate within the area reducing visibility, manoeuvrability, and draining the heat from all who enter. The blizzard itself is maintained with a moderate ongoing mana cost, but the effects it produces are not and will persist after the storms conclusion.

Iron: 
	Your blizzard is small and localised to small 25m radius
```
///
/// details | Cold Snap
	type: minor
	open: True
```text
Violently sap heat from a target, freezing water and otherwise impairing function. Mana cost is relative to the metaphysical weight of the target. 
```
///
#### Moon
/// details | Midnight Eyes
	type: minor
	open: True
```text
Iron:
	See through darkness.

Bronze:
	See through mundane phenomena such as: Fog, Rain, Snow, Sun Glare etc.
```
///
/// details | Soak in Moonlight
	type: minor
	open: True
```text
Absorb moonlight for a variety of beneficial effects. 
Duration of these effects varies greatly on their necessity 

Iron:
	Moonlight greatly increases your recovery speed and mana regeneration. 
```
///
/// details | Eclipse
	type: minor
	open: True
```text
Reduce or negate hostile magical phenomena. 
This ability has a variable mana cost with its efficacy scaling with the relative mana expenditure.
```
///
/// details | Artemis' Blessing
	type: minor
	open: True
```text
The huntress favours you.

Iron:
	Your skill and accuracy with a bow is improved greatly and you benefit more from practice.
	You are proficient with the mundane medical practices of your society.

Bronze:
	Your skill with the bow is innately at the peak for a member of your race, with practice you can ascend beyond.
	For a variable mana cost, you can immediately diagnose medical issues aflicting your target.
```
///
/// details | Lunar Instruments
	type: minor
	open: True
```text
Shape moonlight into semi-stable physical forms. Physical moonlight is incredibly light and is useful in the creation of magical items containing many notable aspects such as magic, cold, healing, and the moon.

Iron:
	Condense moonlight into Liquid, Gas, or Simple Solids.
```
///
#### Friendship
/// details | Voice of the World
	type: minor
	open: True
```text
Speak and understand any language you hear.
With repeated expose to a language, you gradually develop the ability to speak it equivalent to a native of speaker without the use of this ability.
Iron:
	For a small mana cost you can temporarily impart your knowledge of a language to another. Repeated uses of this ability and practice with the language can develop into natural fluency. The subject of this ability must be willing.

Bronze:
	Your understanding is expanded to a limited cultural understanding providing sufficient context to follow another persons expected customs.
	
```
///
/// details | Harmonious Intuition
	type: minor
	open: True
```text
When favourably interacting with another individual your intuitions lead you towards conflict resolution and aid you in building rapport.

Bronze
```
///
/// details | Bigger Picture
	type: minor
	open: True
```text
You are preternaturally capable of understanding a situation in a larger context.

Iron:
	For a small mana cost you may discover previously unknown supplementary information regarding a situation you are observing. This information does not reveal secret or otherwise unknown information and is equivalent to a comprehensive study using public resources.
```
///
/// details | Influential
	type: minor
	open: True
```text
Your words have great impact upon those who accept your influence. 

Iron:
	For a large mana cost you can make a reasonable request of someone whom is then much more likely to carry it out.
	This request must be deemed reasonable and the subject must not flatly reject your influence. 
```
///
/// details | Consideration
	type: minor
	open: True
```text
For a small mana cost you may give another an understanding of your perspective on a discussion. The information given is limited in scope and the subject must be willing.

Bronze
```
///
#### Mercy
/// details | Spare the Dying
	type: minor
	open: True
```text
Heal a target for a small mana cost, with increased effect for those closer to death.

Iron: 
	Healed targets are significantly more resistant to the effects of death magic and are much more capable at surviving in a critical condition.
```
///
/// details | Tranquility
	type: minor
	open: True
```text
Heal a target for a small mana cost, agitated or hostile targets increase this cost.

Iron:  
	Mellows out the target significantly reducing aggression.

Bronze:
	Healing a target applies a buff which applies healing over time, this buff remains until the target is completely healed or takes aggressive action.
```
///
/// details | Sue
	type: minor
	open: True
```text
For a moderate mana cost, manifest a bow of polished ebony wood.
When drawn unloaded Sue manifests an amorphous arrow of sticky shadows, these arrows can heal or harm at your discretion. 
Sue is capable of reshaping herself into a staff if you ask her nicely.

Iron:
	Sue's arrows can be invested with another source of mana causing them to develop additional qualities.
```
///
/// details | Cleanse
	type: minor
	open: True
```text
Iron:
	For a small mana cost cleanse all afflictions from a target.
	For an additional large mana cost, this ability circumvents all effects that prevent cleansing.
	This ability cannot be used on yourself.
```
///
/// details | Judgement
	type: minor
	open: True
```text
For an an extreme mana cost you may isolate yourself and a target for an indefinite period of frozen time.  
While under the effect of Judgement neither you nor the target may take any action to harm or interdict the other nor may either of you travel further than 10 meters apart.
Within Judgement all Lies, Subversions, and Malicious Intents are evident, and all intentions are made clear.
Any information learned within Judgement may never be forgotten.
After a minimum of one subjective hour, you may decide to end the effects of Judgement and release the target from it's control.
You may alternatively decide to execute the target, severing any and all ties, and casting their soul into the astral, preventing all forms of revival.
Higher rank targets instead suffer transcendent damage sufficient to kill someone of your rank.
Regardless of the outcome, Judgement may never be used again on the same target.

Bronze
```
///