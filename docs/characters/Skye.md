# Skye

Cis american lesbian.

# Notable Connections
- [Faith](/characters/Faith) - Girlfriend, Cognate
- [Lucy](/characters/Luciel) - Childhood Friend, Cognate
- [Gregg](/characters/Gregg) - Friend, Cognate
- [Georgia](/characters/Georgia) - Friend, Cognate
- [V / Vai](/characters/V) - Friend, Cognate, Home Server
- [Ash](/characters/Fire Sisters) - Sister, Cognate
- [Lerin](/characters/Fire Sisters) - Sister, Cognate, Creditor
- [Sadie](/characters/Fire Sisters) - Sister, Cognate
- [Ember](/characters/Luciel) - Sister, Cognate
- [Camille](/characters/Camille) - Compatriate, Cognate

# Energy Systems

## Essence User
### Gifts
/// details | Human Ambition
	type: minor
	open: True
```text
Essence abilities advance more quickly.
```
///
/// details | Special Attack Aptitude
	type: minor
	open: True
```text
You are more likely to awaken special attacks than other ability types. Your special attacks have increased effect.
```
///
/// details | Quick Learner
	type: minor
	open: True
```text
You may use skill books for which you meet the requirements.
```
///
/// details | Artificial Mass
	type: minor
	open: True
```text
You are able to control your body's mass freely without changing your other physical properties. 
Gaining mass requires a moderate quantity of mana, however mass may be shed freely.	
```
///
/// details | Glory of Heroes
	type: minor
	open: True
```text
The glory seeks many but few hear it's call. Your sense of purpose is bolstered and resolve increased. 
```
///
/// details | Glamour
	type: minor
	open: True
```text
You may apply subtle illusions to your self to alter your appearance. Applying or changing the glamour costs a small amount of mana but it may then be maintained indefinitely at no cost. When you glamour is challenged, you may maintain it for an ongoing mana cost based on how focused the investigation is and how believable your glamour is.
```
///
### Essences
#### Adept
/// details | Environmental Adaptation
	type: minor
	open: True
```text
Infuse your body with aspects of the environment around you at a low ongoing mana cost. This cost increases if you attempt to maintain environmental aspects into a new environment.

Iron:
	Manifest simple elemental aspects such as water, stone, and fire. 
Bronze:
	Manifest complex environment aspects such as cloud, oil, and metal.
```
///
/// details | Performer
	type: minor
	open: True
```text
You are an experienced performer. When performing for a crowd you instinctively know how to read and direct their mood although you may not necessarily be able to follow your own instincts. 

Iron: 
	You learn new performances quickly and retain those skills for longer.
```
///
/// details | Flexibility
	type: minor
	open: True
```text
Your body is extremely flexible, you suffer no detrimental effects from contorting it and operate at full capacity while doing so.

Iron:
	You are capable of using any limb you possess with the same degree of finesse.
	You are capable of squeezing through any gap larger than the size of your head.

Bronze:
	You are capable of compressing and bending your body into extreme shapes.
```
///
/// details | Way of the Scholar
	type: minor
	open: True
```text
You have a basic proficiency with all martial arts that you have personally engaged in combat with. As you practice and face other practitioners your skill with the respective martial art will improve.

Iron:
	For a moderate mana cost, you can instead learn the basics of a martial art from observation or study. This approach is only capable of teaching you sufficient concepts and theory to begin practising the martial art independently. 
```
///
/// details | Trance
	type: minor
	open: True
```text
For a small ongoing mana cost you can engage yourself in a fugue state dramatically heightening your focus and reaction times.

Iron
```
///
#### Swift
/// details | Stutterstep 
	type: minor
	open: True
```text
Teleport a short distance leaving an after image behind you. As part of this teleportation you can freely redirect yourself.

Bronze
```
///
/// details | Extreme Speed
	type: minor
	open: True
```text
Briefly accelerate to extreme speed before returning to normal. While this ability is active your perception of time is comparatively increased to allow you to function with a degree of normalcy. 

Bronze
```
///
/// details | Impart Momentum
	type: minor
	open: True
```text
For a variable stamina cost transfer a portion of your momentum to a target.
The portion transferred depends on the stamina spent to active the skill and may be limited by your rank.

Iron:
	10,000 kg·m/s Limit (~The momentum of a large american car travelling at 10mph)
```
///
/// details | Conservation of Energy
	type: minor
	open: True
```text
Capture and store a portion of any energy you expend.
This energy persists indefinitely and may be later drawn upon when needed.

Iron:
	0.1% of any energy expended is stored.

Bronze:
	1% of any energy expended is stored.
```
///
/// details | Tireless Effort
	type: minor
	open: True
```text
Spend stamina to supplement any action you take. 
This ability allows you to continue acting at full efficacy up until the moment you collapse.

Bronze
```
///
#### Epic
/// details | Larger than Life
	type: minor
	open: True
```text
Draw attention and bolster your own abilities.
```
///
/// details | Risk Reward 
	type: minor
	open: True
```text
The higher the stakes get, the better you and your allies perform. 

Iron:
	Your aura grants moderate bonuses to all stats to yourself and allies within. This bonus is reduced the further you are from mortal peril.

Bronze:
	Replenish your stamina and mana, Whenever you place yourself in moral peril.
```
///
/// details | Hero's Journey
	type: minor
	open: True
```text
You often find yourself in the thick of things for both good and ill. 

Iron:
	You are more tolerant of hardship and troubling situations.
	You gain an optional resistance to fate related effects.
```
///
/// details | Indomitable
	type: minor
	open: True
```text
Resist the wills of beings far outstripping your power more easily, the larger the power difference, the easier it is to ignore. 		
Ignore the effects of rank disparity for the purposes of affecting higher rank targets.

"In your moment of greatest need, draw upon strength beyond measure."
```
///
/// details | Immortality
	type: minor
	open: True
```text
This ability has no cost but may only be used once every 24 hours. 		

Iron:		
	Instantly restore a large portion of health, mana, and stamina. Amount restored is based on how depleted health, mana, and stamina are when the ability is activated.

Bronze: 
	Gain ongoing health, mana, and stamina recovery effects. The strength of these effects is based on how depleted health, mana, and stamina are when the ability is activated. 
```
///
#### Mystic
/// details | Sight Beyond Soul
	type: minor
	open: True
```text
Iron:
	Perceive auras. 

Bronze:
	Perceive magic.
```
///
/// details | Endless Library
	type: minor
	open: True
```text
Dimensional storage space, exclusively for books or other related media.

Bronze
```
///
/// details | Mana Kinesis
	type: minor
	open: True
```text
Form mana into a physical substance and project it from your body with your movements.

Iron:
	You can convert your mana into stamina freely.
```
///
/// details | Meditate
	type: minor
	open: True
```text
Enter a meditative trance which greatly increases your health recovery and mana regeneration. 
While in this state any ongoing effects such as poisons or enchantments have no effect.
You are passively aware of your surroundings while in this state and will awake to any danger.

Iron:
	Although this state usually takes calm and patience to attain, you may instead force it as a precaution against critical injury. When invoked this way you enter a much deeper and effective trance which you cannot be awoken from until such a time as your injuries have stabilised. Nothing short of dismemberment can kill you while in this state.
```
///
/// details | Consume Soul
	type: minor
	open: True
```text
Take into yourself a portion of another's soul empowering your own abilities while dis-empowering the targets. These effects last only an hour at most and this duration will be reduced by rank disparity, however should the target expire while you still possess a portion of their soul, the effects will become permanent..
This is a transgression upon your targets soul and may have unknown consequences.

Iron
```
///