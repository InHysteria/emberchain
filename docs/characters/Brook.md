# Brook
Once mortal, budding divinity, and friend to Ember. Presently operating as the Goddess [Transition](/characters/Secondary/Transition) on Palimustus.

# Notable Connections
- [Ember](/characters/Ember) - Girlfriend, Premier
- [Ash](/characters/Fire Sisters) - Sister
- [Lerin](/characters/Fire Sisters) - Sister
- [Sadie](/characters/Fire Sisters) - Sister
- [Healer](/characters/secondary/Healer) - Friend, Colleague
- [Knowledge](/characters/secondary/Knowledge) - Colleague

# Energy Systems

## Ascended Divine Being
### Domains
Her nature as [Transition](/characters/Secondary/Transition) has realised her ties to:
`Change (Gradual, Progression, Growth, Death)`

And outside of this her nature, and mythshaping has lead to connections with:
`Fate (Bonds, Connections, Ties)`
