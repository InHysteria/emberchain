# Gregg

Once vanilla benchmark, now going through some things.

# Notable Connections
- [Faith](/characters/Faith) - Childhood Friend, Cognate
- [V / Vai](/characters/V) - Childhood Friend, Cognate, Home Server
- [Lucy](/characters/Luciel) - Friend, Cognate
- [Skye](/characters/Skye) - Friend, Cognate
- [Ember](/characters/Ember) - Weird Friend, Cognate
- [Ash](/characters/Fire Sisters) - Friend, Cognate
- [Lerin](/characters/Fire Sisters) - Weird Friend, Cognate, Creditor
- [Sadie](/characters/Fire Sisters) - Weird Friend, Cognate
- [Chris](/characters/Chris) - Doppleganger, Complicated
- [Georgia](/characters/Georgia) - Very Complicated
- [Camille](/characters/Camille) - Compatriate, Cognate

# Energy Systems

## Essence User
### Gifts
/// details | Human Ambition
	type: minor
	open: True
```text
Essence abilities advance more quickly.
```
///
/// details | Special Attack Aptitude
	type: minor
	open: True
```text
You are more likely to awaken special attacks than other ability types. Your special attacks have increased effect.
```
///
/// details | Unstoppable
	type: minor
	open: True
```text
Movement abilities cannot be negated or impaired. Resonating-force damage and disruptive-force damage are imparted to any obstructing object, increased for each movement ability and special attack in effect. This is a movement effect. 
```
///
/// details | Complimentary
	type: minor
	open: True
```text
Without diminishing your own skills, you tend towards counteracting the weaknesses of those around you. Where your allies are weak, you tend towards strength. 

You have an instinctive sense of how to best cover your allies short comings.
```
///
/// details | Pack Tactics
	type: minor
	open: True
```text
You are skilled at cooperating with allies in melee combat. You can create and exploit openings much easier when working with at least one other ally.
```
///
/// details | 10,000 Lessons
	type: minor
	open: True
```text
Every action can be a lesson and your mind is adapted to learn from them. 
When committing yourself to a task, you are filled with a determination that drives you towards true mastery.
You notice minute details about your performance of any task which you set your mind to improving at, greatly aiding your learning and allowing you to passively improve where others would require active teaching. You also instinctively know how and why these details impacted your performance without being overly critical.
```
///
### Essences
#### Sword
/// details | Swift Strike
	type: minor
	open: True
```text
Empower your strike with mana to deliver a quick attack.

Bronze
```
///
/// details | Lighting Draw
	type: minor
	open: True
```text
From a sheathed position, draw and strike with your weapon in a single powerful motion. 

Bronze
```
///
/// details | Cresent Slash
	type: minor
	open: True
```text
For a small mana cost, project the slash of your weapon outward when making an attack, this slash travels some distance before dissipating.

Iron:
	10m
Bronze:
	50m
```
///
/// details | Sever Ties
	type: minor
	open: True
```text
Make an ethereal slash with various effects.

Iron:
	For a moderate mana cost, attack the connection between a targets body and soul. Weak targets will be killed outright where as strong targets can resist this with mana.
```
///
/// details | Aetherius, Heaven's Blight
	type: minor
	open: True
```text
For a moderate mana cost, summon a long blade.
When you strike a target with Aetherius any flight abilities, natural or otherwise are disabled.

Iron:
	When striking an ungrounded target, they are dragged to the ground and receive resonating force damage in addition to any natural consequence from their plummet.
```
///
#### Mirror
/// details | Mirrors Edge
	type: minor
	open: True
```text
Your agility is improved and you have an innate sense of how to move through and environment.

Iron: 
	Step through reflective surfaces a short distance, with a variable mana cost based on the reflectivity of the surface.
	
Bronze: 
	By rolling with momentum you are capable of dampening the majority of the damage it would've caused.
```
///
/// details | Doppleganger
	type: minor
	open: True
```text
Create an illusory inverted clone of yourself for a very high initial mana cost and a 1 day cooldown. Once created the clone exists until dismissed. 
Without instructions your doppleganger will attempt to act as a supplementary force to you, supporting your attacks or generally acting as your mirror, compensating for where you lack.
The doppleganger is capable of following complex instructions which it tries to carry out faithfully.
You can switch with the doppleganger for a small mana cost, scaling with distance.
The doppleganger can be absored into your body where it boosts your least powerful characteristic, while absorbed in this way it manfiests as a partial inversion of your appearance.

Iron: 
	Your doppleganger is only semi real with limited physical impact, it has access to your essence abilities which it can use independently with it's own cooldowns. The doppleganger lacks vitality and stamina using mana in their place.

Bronze:
	...

```
"He's ranked this up. I'm sure of that, but I'm really not sure what it means. I think us messing with Chris has really thrown all of this out of whack."
- Akashi Codex
```
```
///
/// details | Tesseract
	type: minor
	open: True
```text
You can utilise the mirror dimension as a storage space.

Bronze
```
///
/// details | Hall of Mirrors
	type: minor
	open: True
```text
Fill your environment with temporary reflective surfaces.  

Iron:
	Up to 3 surfaces the size of a typical full length mirror may be created anywhere within your aura.x

Bronze:
	Up to 20 surfaces
```
///
/// details | Shear Plane
	type: minor
	open: True
```text
Perform a special ritual where you shear the mirror plane from a mirror prepared for this purpose. The mirror plane is then formed into a impossibly sharp and fragile knife.

Iron
```
///
#### Myriad
/// details | Thousand Strikes
	type: minor
	open: True
```text
Make continuous quick attacks for a large ongoing stamina cost. 

Bronze
```
///
/// details | Endless Sword
	type: minor
	open: True
```text
For a large ongoing mana cost, strike your weapon and cause various items around you to passively cut anything nearby them. The effected items cut as though it was your own weapon.

Iron: 
	Effects blades, and other sharp cutting instruments.
```
///
/// details | Jacks Gambit
	type: minor
	open: True
```text
You are better at tolerating minor failures, and benefiting from minor successes.

Bronze
```
///
/// details | Too Many Knives
	type: minor
	open: True
```text
For a minuscule mana cost, you may summon enumerable small knives.

Bronze
```
///
/// details | Windsong, The Tranquil End
	type: minor
	open: True
```text
For a moderate mana cost, summon a short blade.
Attacks by Windsong have significantly reduced damage and are painless.

Iron:
	Whenever you land an attack with Windsong, inflict a stack of Analgesia.			

Bronze:
	Analgesia drains stamina from the target. Additional stacks have diminishing effect.

Analgesia:
	Reduced sensation, coordination, and consciousness. Additional instances have a cumulative effect.
```
///
#### Master
/// details | Immortality
	type: minor
	open: True
```text
This ability has no cost but may only be used once every 24 hours.

Iron:
	Instantly restore a large portion of health, mana, and stamina. Amount restored is based on how depleted health, mana, and stamina are when the ability is activated. 

Bronze:
	Gain ongoing health, mana, and stamina recovery effects. The strength of these effects is based on how depleted health, mana, and stamina are when the ability is activated. 
```
///
/// details | Competency
	type: minor
	open: True
```text
Allies within your aura can call upon your mastery to a limited degree.
Whenever they attempt a task they can rely upon your skill with it to bolster their own.

Bronze
```
///
/// details | Sage's Guidance
	type: minor
	open: True
```text
When teaching others, your lessons have greater impact and you instinctively know what areas others need to improve on.

Iron
```
///
/// details | Prestige
	type: minor
	open: True
```text
For a large mana cost, forget everything you know on a specific topic, losing muscle memory and gained skill with the topic. Your innate talent with the topic is improved proportionally to the experience lost.

Iron:
	The rate at which you develop skills is improved.		
Bronze:
	For a very large mana ongoing cost, temporarily manfiest all lost memory and skill with a topic.
```
///
/// details | Vow
	type: minor
	open: True
```text
For a very large mana cost, summon a blade.
You may only draw Vow with a promise of your intentions and a moderate mana cost, any individuals this promise pertains to are aware of it in its entirety. 
When Vow is sheathed, if you have fulfilled your promise, it is permanently invested with a portion of mana causing it to deal additional transcendent damage.
The more challenging your promise was to achieve, the more mana is invested in Vow. 
If you have not fulfilled your promise, your mana is permanently reduced by a small amount.
The more effort you expended towards acheiving your promise, the smaller this amount.

Iron
```
///