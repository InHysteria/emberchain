# Ember
Beneficiary of [Jumpchan's](/redacted) attentions, Ember finds herself in the all too usual positions of Guide, Friend, and General Nuisance. Friend to many, Ember seems to be finding herself the Nexus of changing times. 

# Notable Connections
- [Brook](/characters/Brook) - Girlfriend, Goddess
- [Ash](/characters/Fire Sisters) - Sister, Cognate
- [Lerin](/characters/Fire Sisters) - Sister, Cognate, Creditor
- [Sadie](/characters/Fire Sisters) - Sister, Cognate
- [Faith](/characters/Faith) - Sister, Cognate
- [Lucy](/characters/Luciel) - Sibling, Cognate
- [Skye](/characters/Skye) - Cousin, Cognate
- [Gregg](/characters/Gregg) - Cousin, Cognate
- [Georgia](/characters/Georgia) - Cousin, Cognate
- [V / Vai](/characters/V) - Siblings, Cognate, Home Server
- [Camille](/characters/Camille) - Cousin, Cognate

## Inventory
- Money crystal xeltreas
- 10000's of USD
- 100's of Lesser Sprit Coins
- 10's of Iron Spirit Coins
- Mana Condenser, Iron
- ~200 Steel Quintessence
- ~50 Technology Quintessence
- ~150 Toxic Quintessence
- (Assorted 7-11 Items)
- (Remains of original life)
# Energy Systems
/// details | Hexcasting
	open: True
Leveraging the power of her mind, Ember is able to form intricate patterns of Axie to perform a form of magic relying on Mind Authority. Things clearly within the domain of the mind are cheap and possible with Hexcasting (that does not mean they are simple), stretching this definition it is possible todo things with some, tentative relation to the mind, applying force was once thought to be the limit of this rule bending but Ember has different opions about such limits..
///
## Essence User
"We shape the clay, but it is the emptiness that makes it useful." - Lao Tzu
/// details | Exchange Fates
	type: minor
	open: True
[=74.0% "Iron 7"]
```text
Swap the fates of two individuals for a period of time, allowing you to redirect consequences.
This ability has a variable mana cost based on the viability of your changes.
Animate entities and especially those possessing souls will instinctively resist this with the latter potentially treating this as a transgression.
Iron:
	Exchange 10 seconds of fate.
```
///
/// details | Entwine Fate
	type: minor
	open: True
[=96.0% "Iron 9"]
```text
Share resources, boons, and afflictions amongst a group. This ability consumes a large ongoing mana cost reduced by proximity and inter group connection.

Iron:
	Evenly and automatically distribute any consequences amongst group members.		
```
///
/// details | **The Spider's Web**
	type: minor
	open: True
[=100.0% "Diamond 10"]
```text
Weave fate.
This ability has been empowered by The Fox granting it effects far outside your rank.

Iron:
	Direct local fate towards a destination. This effect consumes moderate mana while it is in effect. 
Bronze:
	Make a single negative statement about fate, for an overwhelming mana cost. 
	The statement must be viable and not conflict with any other statement made about fate.
Silver:
	Make a single positive statement about fate, for an all-consuming mana cost.
	The statement must be viable and not conflict with any other statement made about fate.
Gold:
	Take another's thread of fate for yourself, for a suicidal mana cost. Taking fate in this mana will outright kill weak individuals.
	Targets will instinctively resist this and see your attempt as a transgression.
Diamond:
	Adapt the weave, for a variable mana cost.
	"I would ***really*** advise against using this again, I'm fairly sure that you used almost the minimum of it's extents and the cost almost tore us apart."
```
///
/// details | Create Myth
	type: minor
	open: True
[=46% "Iron 4"]
```text
The stories you tell spread easily and gain a sense of truth to them as they spread.

Iron: 
	Create a meme which inexorably spreads throughout a community.
```
///
/// details | Fates Aegis
	type: minor
	open: True
[=79.1% "Iron 7"]
```text
Direct and individuals fate away from harm.

Iron: 
	Small ongoing mana cost, Weak efficacy
```
///