# Wanderlust
Jumpchan's Key

# Facets
- Jumpchan owns Wanderlust.
	- Simple possession of the key is insufficient to establish a claim to ownership.
	- The owner (Jumpchan) may award privileges to others such that they may use some or all of Wanderlust's capabilities while they possess it.
- Wanderlust is at least one key.
	- When Wanderlust is many keys, all are equally Wanderlust.
- Wanderlust grants the bearer the right to create a way between, the hows and whys are explicitly defined but generally permissive so long as the intent follows some rules.
	- The way bridges two well defined points.
	- The must be an internally consistent transition.
		- The doorways could be the same sort of thing, with the same general shape, in the same conceptual layer.
		- Alternatively some medium must be provided to act as an intermediary.
	- The way formed is the smallest possible liminal space spanning both portals.
		- If for example two simple wood framed doors were spanned, then the way would be only as long as the thickness of the door and no more.
- Wanderlust was originally created as the "The Key to The Way" granting the holder absolute authority over it.
	- Wanderlust has since been reforged to serve it's new purpose.


"The hammers.. I feel them still" - Akashic Codex